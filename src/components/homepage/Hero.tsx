import clsx from "clsx";
import React, { FunctionComponent } from "react";

import styles from "./Hero.module.scss";

export const Hero: FunctionComponent = () => {
  return (
    <header className={clsx("hero hero--primary", styles.heroBanner)}>
      <div className="container">
        <h1 className={clsx("hero__title", styles.title)}>
          Hi. I'm <span className={styles.highlighted}>Anoop</span>,
          <br />
          Computational chemist.
        </h1>
        <p className={clsx("hero__subtitle", styles.subtitle)}>
          Software engineer interested in creating computational codes.
          <br /> FOSS enthusiast.
        </p>
      </div>
    </header>
  );
};
