import './css/project.css';

# **Q-Blink:** TCSPC Change-Point Detection Tool

<div style={{ textAlign: "justify", padding:"10px"}}>
Analyze photon arrival time data to detect change points with precision, enabling detailed insights into time-resolved photon detection events. Designed specifically for Time-Correlated Single Photon Counting (TCSPC) data, this tool helps researchers uncover significant transitions and patterns in photon intensity distributions. The project was developed as a closed-source engine during my time at <a href="https://www.iisertvm.ac.in/">IISER-Thiruvananthapuram </a>. But I wanted to show-case the usability through a <b>flask</b> web-app</div>.

**Note:** Currently the web-app works with <a href="https://github.com/PicoQuant/PicoQuant-Time-Tagged-File-Format-Demos">PTU file formats </a>.

<a href="https://cpawebapp.pythonanywhere.com/" target="_blank" >
  <button class="custom-button" style={{ textDecoration: "none" }}>
    Try out the web-app!!
  </button>
</a>

![Q-Blink](./pics/qblink.jpg)

<section id="citation">
    <h2>Please Cite This Work</h2>
    <p>If you use this tool in your research, please cite it as follows:</p>
    <blockquote cite="https://doi.org/10.1234/example">
        "E Krishnan Vishnu, Anoop Ajaya Kumar Nair, K George Thomas, Core-Size-Dependent Trapping and Detrapping Dynamics in CdSe/CdS/ZnS Quantum Dots, JPC-C, 125(46), 2021, DOI: <a href='https://doi.org/10.1021/acs.jpcc.1c08137' target="_blank" rel="noopener">10.1021/acs.jpcc.1c08137</a>"
    </blockquote>
    <blockquote cite="https://doi.org/10.1234/example">
        "Arghyadeep Garai, E Krishnan Vishnu, Souvik Banerjee, Anoop Ajaya Kumar Nair, Suman Bera, K George Thomas, Narayan Pradhan, Vertex-Oriented Cube-Connected Pattern in CsPbBr3 Perovskite Nanorods and Their Optical Properties: An Ensemble to Single-Particle Study, JACS, 145(25), 2023, DOI: <a href='https://doi.org/10.1021/jacs.3c03759' target="_blank" rel="noopener">10.1021/jacs.3c03759</a>"
    </blockquote>
</section>
