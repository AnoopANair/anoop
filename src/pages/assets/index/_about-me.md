I&nbsp;write efficient simulation engines in **C++** and modern interfaces using **Python** (**Pybind11**). In addition, i create webapps for scientists.

I have seven years of research, data analysis and programming expertise in the topics of
software development, FOSS, machine learning, optimization and statistics. I’m enthusiastic
to work on both research and non-research oriented problems and find efficient answers to
them.

In 2021, I&nbsp;have completed a&nbsp;master's in Physics at Indian Institute of Science Education and Research - Thiruvananthapuram.
