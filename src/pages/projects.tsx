import React from "react";
import Layout from "@theme/Layout";

import { Project, ProjectData } from "../components/projects/Project";

const assetsDir = "./assets/projects";
const projects: ProjectData[] = [
  {
    title: "SCME-CPP",
    description:
      "A C++/python package for efficient and accurate solvent simulations",
    url: "./assets/projectDetails/SCMECpp",
    image: require(`${assetsDir}/ScmeCPP.png`),
    role: "Author",
  },
  {
    title: "H-MPol",
    description:
      "A python module to analyse quantum-dot intermittency data",
    url: "./assets/projectDetails/HMPol",
    image: require(`${assetsDir}/HMPOL.png`),
    role: "Author",
  },
  {
    title: "Quantum-Blink",
    description:
      "A python module to analyse quantum-dot intermittency data",
    url: "./assets/projectDetails/QBlink",
    image: require(`${assetsDir}/QBlink.png`),
    role: "Author",
  },
];

const title = "Projects";
const description = "Featured projects I was/am involved in.";

export default function Projects(): JSX.Element {
  return (
    <Layout title={title} description={description}>
      <main className="container container--fluid margin-vert--lg">
        <h1>{title}</h1>
        <p>{description}</p>

        <div className="row">
          {projects.map((project) => (
            <Project key={project.title} {...project} />
          ))}
        </div>
      </main>
    </Layout>
  );
}
