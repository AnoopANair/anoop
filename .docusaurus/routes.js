import React from "react";
import ComponentCreator from "@docusaurus/ComponentCreator";

export default [
  {
    path: "/__docusaurus/debug",
    component: ComponentCreator("/__docusaurus/debug", "fc6"),
    exact: true,
  },
  {
    path: "/__docusaurus/debug/config",
    component: ComponentCreator("/__docusaurus/debug/config", "bc6"),
    exact: true,
  },
  {
    path: "/__docusaurus/debug/content",
    component: ComponentCreator("/__docusaurus/debug/content", "79a"),
    exact: true,
  },
  {
    path: "/__docusaurus/debug/globalData",
    component: ComponentCreator("/__docusaurus/debug/globalData", "66c"),
    exact: true,
  },
  {
    path: "/__docusaurus/debug/metadata",
    component: ComponentCreator("/__docusaurus/debug/metadata", "98c"),
    exact: true,
  },
  {
    path: "/__docusaurus/debug/registry",
    component: ComponentCreator("/__docusaurus/debug/registry", "c4e"),
    exact: true,
  },
  {
    path: "/__docusaurus/debug/routes",
    component: ComponentCreator("/__docusaurus/debug/routes", "813"),
    exact: true,
  },
  {
    path: "/assets/projectDetails/HMPol",
    component: ComponentCreator("/assets/projectDetails/HMPol", "884"),
    exact: true,
  },
  {
    path: "/assets/projectDetails/QBlink",
    component: ComponentCreator("/assets/projectDetails/QBlink", "640"),
    exact: true,
  },
  {
    path: "/assets/projectDetails/SCMECpp",
    component: ComponentCreator("/assets/projectDetails/SCMECpp", "643"),
    exact: true,
  },
  {
    path: "/blog",
    component: ComponentCreator("/blog", "f7c"),
    exact: true,
  },
  {
    path: "/blog/archive",
    component: ComponentCreator("/blog/archive", "6b4"),
    exact: true,
  },
  {
    path: "/blog/hello-world",
    component: ComponentCreator("/blog/hello-world", "ef4"),
    exact: true,
  },
  {
    path: "/blog/tags",
    component: ComponentCreator("/blog/tags", "499"),
    exact: true,
  },
  {
    path: "/blog/tags/hello",
    component: ComponentCreator("/blog/tags/hello", "bb8"),
    exact: true,
  },
  {
    path: "/blog/tags/introduction",
    component: ComponentCreator("/blog/tags/introduction", "1ea"),
    exact: true,
  },
  {
    path: "/blog/tags/shorts",
    component: ComponentCreator("/blog/tags/shorts", "cf6"),
    exact: true,
  },
  {
    path: "/blog/tags/world",
    component: ComponentCreator("/blog/tags/world", "22d"),
    exact: true,
  },
  {
    path: "/projects",
    component: ComponentCreator("/projects", "ebe"),
    exact: true,
  },
  {
    path: "/publications",
    component: ComponentCreator("/publications", "3e5"),
    exact: true,
  },
  {
    path: "/search",
    component: ComponentCreator("/search", "357"),
    exact: true,
  },
  {
    path: "/talks",
    component: ComponentCreator("/talks", "610"),
    exact: true,
  },
  {
    path: "/",
    component: ComponentCreator("/", "4ce"),
    exact: true,
  },
  {
    path: "*",
    component: ComponentCreator("*"),
  },
];
